import sys, os, re

if len(sys.argv) < 2:
	sys.exit("Usage: %s filename" % sys.argv[0])

filename = sys.argv[1]

if not os.path.exists(filename):
	sys.exit("Error: File '%s' not found" % sys.argv[1])

class Player:
    def __init__(self, name):
        self.name = name
        self.at_bats = 0
        self.hits = 0
        self.runs = 0

    def playGame(self, at_bats, hits, runs):
        self.at_bats += int(at_bats)
        self.hits += int(hits)
        self.runs += int(runs)

    def getBattingAverage(self):
        self.battingAverage = self.hits * 1.0 / self.at_bats

    def printStats(self):
        print(self.name + ': ' + '{0:.3f}'.format(self.battingAverage))


f = open(filename)
regex = re.compile(r'\b(?P<name>.+) batted (?P<at_bats>\d+) times with (?P<hits>\d+) hits and (?P<runs>\d+) runs\b')
players = []
for line in f:
    match = re.search(regex, line)
    if match is not None:
        player = [player for player in players if player.name == match.group('name')]
        if(len(player) > 0):
            player[0].playGame(match.group('at_bats'), match.group('hits'), match.group('runs'))
        else:
            player = Player(match.group('name'))
            players.append(player)
            player.playGame(match.group('at_bats'), match.group('hits'), match.group('runs'))

[x.getBattingAverage() for x in players]
players.sort(key=lambda player: player.battingAverage, reverse=True)
[x.printStats() for x in players]
